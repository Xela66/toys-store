<?php
require_once '_init.php';

$url_params = array_keys($_GET);

if (
    (!in_array('tid', $url_params) || preg_match(REGEX_ID, $_GET['tid']) === 0)
    || (in_array('sid', $url_params) && preg_match('/\d+/', $_GET['sid']) === 0)
) {
    renderErrorPage(400);
    die();
}

$tid = $_GET['tid'];
$sid = $_GET['sid'] ?? null;

$toy = null;

$arr_q_toy = [];

$prep_types = '';

$prep_args = [];

$arr_q_toy[] = 'SELECT';
$arr_q_toy[] = 'toys.id, toys.name, toys.description, toys.name, toys.price, toys.image,';
$arr_q_toy[] = 'brands.name AS brand, SUM(stock.quantity) AS stock';
$arr_q_toy[] = 'FROM toys';
$arr_q_toy[] = 'JOIN brands ON toys.brand_id = brands.id';
$arr_q_toy[] = 'JOIN stock ON toys.id = stock.toy_id';


if (!is_null($sid) && $sid > 0) {
    $arr_q_toy[] = 'AND stock.store_id = ?';
    $prep_types .= 'i';
    $prep_args[] = &$sid;
}

$arr_q_toy[] = 'WHERE toys.id = ?';
$prep_types .= 'i';
$prep_args[] = &$tid;

$arr_q_toy[] = 'GROUP BY toys.id';
$stmt = mysqli_prepare($mysqli, implode(' ', $arr_q_toy));

if ($stmt) {
    mysqli_stmt_bind_param($stmt, $prep_types, ...$prep_args);

    mysqli_stmt_execute($stmt);

    $result_toys = mysqli_stmt_get_result($stmt);
    mysqli_stmt_close($stmt);

    $toy = mysqli_fetch_assoc($result_toys);
}

if (is_null($toy)) {
    renderErrorPage(404);
    die();
}

$page_title = $toy['name'];
