<?php
require_once '_init.php';

$page_title = 'N°1 de la vente de jouet';

$toys = [];

$q_toys = 'SELECT
			   toys.id,
			   toys.name,
			   toys.price,
			   toys.image
					FROM toys
					JOIN sales ON sales.toy_id = toys.id
				GROUP BY toys.id, toys.price
				ORDER BY SUM(sales.quantity) DESC, toys.price DESC
				LIMIT 3';

if (!$result_toys = mysqli_query($mysqli, $q_toys)) return;

while ($data_toy = mysqli_fetch_assoc($result_toys)) $toys[] = $data_toy;
