<?php
require_once '_init.php';

$page_title = 'Liste de nos jouets';
$url_params = array_keys($_GET);

if (
    (in_array('bid', $url_params) && preg_match('/\d+/', $_GET['bid']) === 0)
) {
    renderErrorPage(400);
    die();
}

$bid = $_GET['bid'] ?? null;

$toys = [];
$arr_q_toys = [];

$prep_types = '';

$prep_args = [];

$arr_q_toys[] = 'SELECT';
$arr_q_toys[] = 'toys.id, toys.name, toys.name, toys.price, toys.image';
$arr_q_toys[] = 'FROM toys';

if (!is_null($bid) && $bid > 0) {
    $arr_q_toys[] = 'WHERE toys.brand_id = ?';
    $prep_types .= 'i';
    $prep_args[] = &$bid;
}

$stmt = mysqli_prepare($mysqli, implode(' ', $arr_q_toys));

if ($stmt) {
    if (!empty($prep_args)) {
        mysqli_stmt_bind_param($stmt, $prep_types, ...$prep_args);
    }

    mysqli_stmt_execute($stmt);

    $result_toys = mysqli_stmt_get_result($stmt);
    mysqli_stmt_close($stmt);

    while ($toy = mysqli_fetch_assoc($result_toys)) $toys[] = $toy;
}
