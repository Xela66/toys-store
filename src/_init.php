<?php
const PER_PAGE = 4;

const REGEX_ID = '/^[1-9]\d*$/';

const DB_HOST = 'database';
const DB_USER = 'lamp';
const DB_PASS = 'lamp';
const DB_NAME = 'lamp';

$mysqli = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

$page_title = 'Insérer le nom de la page';

function float2Currency(float $value, string $locale = 'fr_FR', string $currency = 'EUR'): string
{
    $fmt = numfmt_create($locale, NumberFormatter::CURRENCY);

    return numfmt_format_currency($fmt, $value, $currency);
}

function renderBrandsMenuItems(): void
{
    global $mysqli;

    $html = '';

    $q = 'SELECT brands.*, count(brands.id) as refs
			FROM toys
			JOIN brands ON brands.id = toys.brand_id
			GROUP BY brands.id';

    $result = mysqli_query($mysqli, $q);

    if (!$result) return;

    while ($brand = mysqli_fetch_assoc($result)) {
        $html .= sprintf('<li><a href="/liste.php?bid=%s">%s (%s)</a></li>', $brand['id'], $brand['name'], $brand['refs']);
    }

    echo $html;
}

function renderToyItem(array $toy): void
{
    $format = '<li class="toy-item">';
    $format .= '<a href="/details.php?tid=%1$s">';
    $format .= '<h2>%2$s</h2>';
    $format .= '<p class="image">';
    $format .= '<img src="/img/toys/%3$s" alt="%2$s">';
    $format .= '</p>';
    $format .= '<p class="price">%4$s</p>';
    $format .= '</a>';
    $format .= '</li>';


    printf(
        $format,
        $toy['id'],
        $toy['name'],
        $toy['image'],
        float2Currency(floatval($toy['price']))
    );
}

function renderBrandsOptions(?int $bid): void
{
    global $mysqli;

    if (is_null($bid)) $bid = 0;

    $q = 'SELECT * FROM brands';
    $result = mysqli_query($mysqli, $q);

    if (!$result) return;

    echo '<option value="0">Quel marque ?</option>';

    while ($brand = mysqli_fetch_assoc($result)) {
        $selected = intval($brand['id']) === $bid ? ' selected' : '';

        printf('<option value="%s"%s>%s</option>', $brand['id'], $selected, $brand['name']);
    }
}

function renderStoresOptions(?int $sid): void
{
    global $mysqli;

    if (is_null($sid)) $sid = 0;

    $q = 'SELECT * FROM stores';
    $result = mysqli_query($mysqli, $q);

    if (!$result) return;

    echo '<option value="0">---- Selectionner un magasin ----</option>';

    while ($store = mysqli_fetch_assoc($result)) {
        $selected = intval($store['id']) === $sid ? ' selected' : '';

        printf('<option value="%s"%s>%s</option>', $store['id'], $selected, $store['name']);
    }
}

function renderPagination(string $base_url, int $current_page, int $max_page): void
{
    $result_format = '<div class="filter-bar">%s</div>';

    $str_content = $current_page > 1
        ? sprintf('<a href="%s?p=%s">Page précédente</a>', $base_url, $current_page - 1)
        : '<span>Page précédente</span>';

    $str_content .= sprintf('<span class="counter">%s / %s</span>', $current_page, $max_page);

    $str_content .= $current_page < $max_page
        ? sprintf('<a href="%s?p=%s">Page suivante</a>', $base_url, $current_page + 1)
        : '<span>Page suivante</span>';

    printf($result_format, $str_content);
}

function renderErrorPage(int $http_status_code = 404): void
{
    $message = '';

    switch ($http_status_code) {
        case 404:
            $message = 'Cette page n\'existe pas !';
            break;
        default:
            $message = 'Une erreur s\'est produite';
            break;
    }

    http_response_code($http_status_code);

    $page_title = $message;

    require_once './partials/top.html.php';

    printf('<h1>%s</h1>', $message);

    require_once './partials/bottom.html.php';
}