<?php
require_once './src/liste.php';
require_once './partials/top.html.php';
?>
    <h1>Nos jouets</h1>

    <div class="filter-bar">
        <form action="" method="get">
            <select name="bid">
                <?php renderBrandsOptions($bid) ?>
            </select>

            <input type="submit" value="Ok">
        </form>
    </div>

<?php renderPagination('/liste.php', 1, 5) ?>

<?php if (empty($toys)): ?>
    <div class="no-item">Aucun résultat</div>
<?php else: ?>
    <ul class="toys-list">
        <?php foreach ($toys as $toy) renderToyItem($toy) ?>
    </ul>
<?php
endif;

require_once './partials/bottom.html.php';
