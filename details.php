<?php
require_once './src/details.php';
require_once './partials/top.html.php';
?>
    <h1><?php echo $toy['name'] ?></h1>
    <div class="toy-details">
        <div class="image-col">
            <p class="image">
                <img src="/img/toys/<?php echo $toy['image'] ?>" alt="<?php echo $toy['name'] ?>">
            </p>

            <p class="price">
                <?php echo float2Currency(floatval($toy['price'])) ?>
            </p>

            <form action="" method="get">
                <input type="hidden" name="tid" value="<?php echo $toy['id'] ?>">

                <select name="sid">
                    <?php renderStoresOptions($sid) ?>
                </select>

                <input type="submit" value="Ok">
            </form>

            <p class="labeled-value">
                stock: <span><?php echo $toy['stock'] ?></span>
            </p>
        </div>

        <div class="text-col">
            <p class="labeled-value">
                Marque: <span><?php echo $toy['brand'] ?></span>
            </p>
            <div class="toy-description"><?php echo $toy['description'] ?></div>
        </div>
    </div>
<?php
require_once './partials/bottom.html.php';
