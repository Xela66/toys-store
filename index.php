<?php
require_once './src/index.php';
require_once './partials/top.html.php';
?>

    <h1>Top 3 des ventes</h1>

<?php if (empty($toys)): ?>
    <div class="no-item">Aucun résultat</div>
    <?php return; endif ?>

    <ul class="toys-list">
        <?php foreach ($toys as $toy) renderToyItem($toy) ?>
    </ul>

<?php
require_once './partials/bottom.html.php';
