<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $page_title ?> - ToysRus.com</title>

    <link rel="stylesheet" href="style.css">
</head>
<body>
<div class="container">
    <header>
        <div class="logo">
            <a href="/">
                <img src="./img/logo.jpg" alt="ToysRUs">
            </a>
        </div>
        <nav>
            <ul>
                <li><a href="/liste.php">Tous les jouets</a></li>
                <li>
                    <span>Par marque</span>
                    <ul><?php renderBrandsMenuItems() ?></ul>
                </li>
            </ul>
        </nav>
    </header>

    <main>
